import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        //introducere text de la consola
        System.out.println("Introdu text");
        String text = s.nextLine();

        //introducere int de la consola

        System.out.println("Introdu int-ul");
        int a = s.nextInt();

        //afisare text si int concatenate
        System.out.println("ai introdus textul " + text + " " + "si int-ul " + a);
        //comentariu2
    }
}
