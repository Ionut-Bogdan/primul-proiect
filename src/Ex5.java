import java.util.Scanner;

public class Ex5 {

    public static void main(String[] args) {
        // meniu de produse pe care il afisam
        //utilizatorul alege
        //pretul -> 10
        //10 -> succes
        //daca pretul nu este corect reintra in bucla

        Scanner s = new Scanner(System.in);

        String meniu = "1. Water\n2. Fanta\n3. Coca Cola";
        displayMessage(meniu);

        int productCode = selectProduct();
        int price = getPrice(productCode);

        displayMessage("Pretul produsului este: " + price);

        pay(price);
    }

    public static void displayMessage(String message) {
        System.out.println(message);
    }

    public static int selectProduct() {
        displayMessage("Introduceti numarul bauturii:");
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();

        if (x != 1 && x != 2 && x != 3) {
            displayMessage("Optiunea nu este valida");
            System.exit(0);
        }
        return x;
    }

    public static void pay(int price) {
        Scanner s = new Scanner(System.in);
        int y;
        do {
            displayMessage("Introduceti contravaloarea bauturii!");
            y = s.nextInt();
        } while (y != price);
        displayMessage("Succes");
    }

    public static int getPrice(int code) {
        switch (code) {
            case 1:
                return 3;
            case 2:
                return 5;
            case 3:
                return 10;
            default:
                return 0;

        }
    }
}
