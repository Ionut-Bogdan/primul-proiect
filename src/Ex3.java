import java.util.Scanner;

public class Ex3 {
    public static void main(String[] args) {
        // Citesc un sir de nr intregi pe care le pun in array si apoi afisare array
        int[] arr = new int[100];
        System.out.println("Introdu nr :");
        Scanner s = new Scanner(System.in);
        int x = s.nextInt();
        int index = 0;
        while (x != 0 && index < arr.length) {
            arr[index] = x;
            x = s.nextInt();
            index++;
        }
        System.out.print("Array este : [");
        for (int i = 0; i < index; i++) {
            System.out.print(arr[i] );
            if(i != index - 1){
                System.out.print(", ");
            }
        }
        System.out.print("]");



    }
}
